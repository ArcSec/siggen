* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*                                     /mhs+/-`                                                                          *
*                                     /MMMMMMMNdhs+:.`                                                                  *
*                                     /MMMMMMMMMMMMMMMNdyo+:`                                                           *
*                                     /MMMMMMMMMMMMMMMMMMMMh`.s.                                                        *
*                                     -dmmNNMMMMMMMMMMMMMMs -NMMs`                                                      *
*                                                          /MMMMMN+                                                     *
*                                    oNmmddhhhyyssoo++/-  oMMMMMMMMm/                                                   *
*                                  .dMMMMMMMMMMMMMMMMMd` hMMMMMMMMMMMm:                                                 *
*                                 +NMMMMMMMMMMMMMMMMMy .dMMMMMMMMMMMMMMd-                                               *
*                               .hMMMMMMMMMMMMMMMMMM+ -NMMMMMMMMMMMMMMMMMy.                                             *
*                              /NMMMMMMMMMMMMMMMMMN: /MMMMMMMMMMMMMMMMMMMMMs`                                           *
*                            `hMMMMMMMMMMMMMMMMMMm. oMMMMMMMMMMMMMMMMMMMMMMMNo`                                         *
*                           /NMMMMMMMMMMMMMMMMMMh` hMMMMMMMMMMMMMMMMMMMMMMMMMMN+   .-:://+oo+                   .:/`    *
*                         `yMMMMMMMMMMMMMMMMMMMs .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm\ .yMMMMMMo              `-+sdNNo`     *
*                        :NMMMMMMMMMMMMMMMMMMM/ -NMMMMMMMMMMMMMMMMMMMMMMMMM+oNMMMMd: -hMMm-          ./ohmMMMMMy`       *
*                      `sMMMMMMMMMMMMMMMMMMMN- /MMMMMMMMMMMMMMMMMMMMMMMMMMMm. +NMMMMh- -o`     `-+sdNMMMMMMMMh-         *
*                     -mMMMMMMMMMMMMMMMMMMMd` sMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  /NMMMMy.   /ohmMMMMMMMMMMMMm:           *
*                    sMMMMMMMMMMMMMMMMMMMMy `hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNydMMMMMMMs` -dMMMMMMMMMMMMN+             *
*                  -mMMMMMMMMMMMMMMMMMMMMs  :hNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNo` :dMMMMMMMMMo`              *
*                 oMMMMMMMMMMMMMMMMMMMMMMMmy.  .+ymMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  \mMMMMMy.                *
*               .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMNy+. .\smMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm\  +NMd-                  *
*              +MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNy+- `:sdMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd- `-                    *
*            .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNh+- `:ohNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh.                     *
*           +NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmdhyso+/:-.`      -+yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMd+.                     *
*         `hMMMMMMMMMMMMMMMMMMMNmdhyso+/:-.`                        .\ymMMMMMMMMMMMMMMMMMMMMMh+`                        *
*        /NMMMMMMNmdhyso+/:-.`                                          .\sdMMMMMMMMMMMMMMh/`                           *
*       -+//:-.`                                                            `:odMMMMMMNy/`                              *
*                            .NMMN'                                             `-ohy:                                  *
*                     /++++oNMMMN/                                                                                      *
*                     .dMMMMMMMd.                                                                                       *
*                       hMMMMMh                                                                                         *
*                     `sMMMMMMMs`            `MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  -MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm       *
*                    -mMMMMdMMMMm-           `MMMMMMMMMMMMMMMMMMMMMMMMMMMMMd.   -MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm       *
*                   +MMMMN: :NMMMMo           .......................-hMMN+     -MMM+........................hMMm       *
*                 .hMMMMh`   `hMMMMh.         ///////////////////////dMMh.      -MMM:                        +yys       *
*                /NMMMM+       +MMMMN/       `MMMMMMMMMMMMMMMMMMMMMMMMM+        -MMM:                                   *
*              `yMMMMd.         .dMMMMy`     `MMMNdddddddddddddddddddMMm-       -MMM:                                   *
*             -mMMMMs:::::::::::-:dMMMMm-    `MMMy                   -mMMs`     -MMM:                                   *
*            oMMMMm/yMMMMMMMMMMMMMMMMMMMMo   `MMMy                    `sMMm:    -MMMhssssssssssssssss:                  *
*          .dMMMMy:mMMMMMMMMMMMMMMMMMMMMMMd. `MMMh                      :mMMy`  -MMMMMMMMMMMMMMMMMMMMo                  *
*         /NMMMN/.ssssssssssssssyyyyyyyyyyyy.`syy+                       `oyso` .ssssssssssssyyyysyyy/                  *
*       `hMMMMd.                                                                                                        *
*      :NMMMMo   /mmsssssssso  dMyssssss/  /hdysssssss  Mm        NM  NMsssssssymh-  MN .ssssssssss. dm\    /md/        *
*     sMMMMm-    :dmyyyyyyyo:  dM.oooooo` oMo           Mm        NM  NM       `oMs  MN      NM       :hNo+md:          *
*   .mMMMMy`         ``````+M+ dM`        :Nh-          mM:      :Md  NM yyyydMMy    MN      NM         -NM:            *
*  /mNNNm/       :yyyyyyyyhy+` ydhyyyyyy/  `/shyyyyyyy  `/shyyyyhs/`  hh      .odo.  dh      hd          hd             *
*                                                                                                                       *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Title         : SigGen                                                                                                *
* Link          : https://bitbucket.org/ArcSec/siggen                                                                   *
* Authors       : Siegen                                                                                                *
* Organization  : Arc Security, http://arcsecurity.org                                                                  *
* Date          : 20140608                                                                                              *
* License       : (c) 2014 Arc Security, MIT License, http://opensource.org/licenses/MIT                                *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Description:

This is the tool used to create standard corporate signatures for ArcSec members. It does so by using official member
data from the ArcSec forums. It can also be automated to keep member signatures up-to-date with a member's rank or any
template changes without the members having to change any URLs.


Usage: