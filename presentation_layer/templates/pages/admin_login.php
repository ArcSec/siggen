
<h1 class="page-title ">

    <span class="top-line"></span>
    <span class="inner-line"></span>
    <span class="icon"></span>
        Arc Security Signature Generator - Admin Login
    <span class="bottom-line"></span>

</h1>

<div class="panel">
    
    <div class="content-block" style="font-size:80%;">
        <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">Home</a> 
        &gt; <a href="index.php">Signature Generator</a>
        &gt; Admin
        
        <a href="admin.php" style="float:right;">Admin</a> 

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    </div>
    
    <div class="content-block">
        
        <img src="presentation_layer/images/logo.png" class="logo">
        
        <p>Pleas login using your ArcSec forum credentials.</p>
        
        <br><br><br>

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    
    </div>

    
    <div class="content-block">
        
        <div class="sig_generator_form_bounds">
            <form method="POST" id="sig_generator_auth_form" name="sig_generator_auth_form">
                
                <center>
                    
                    <input type="text" id="username" name="username" placeholder="Username">
                    
                    <input type="password" id="password" name="password" style="margin-left:10%;">

                    <br>
                    <br>
                    
                    <a onClick="document.getElementById('sig_generator_auth_form').submit();" id="submit" class="holobtn">
                        <span class="holobtn-top abs-overlay trans-02s">Login</span>
                        <span class="holobtn-bottom abs-overlay trans-02s"></span>
                    </a>
                </center>
                
            </form>
        </div>

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    
    </div>
    
</div>