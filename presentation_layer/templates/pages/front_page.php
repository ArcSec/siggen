
<h1 class="page-title">

    <span class="top-line"></span>
    <span class="inner-line"></span>
    <span class="icon"></span>
        Arc Security Signature Generator
    <span class="bottom-line"></span>

</h1>

<div class="panel">
    
    <div class="content-block" style="font-size:80%;">
        <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">Home</a> 
        &gt; Signature Generator
        
        <a href="admin.php" style="float:right;">Admin</a> 

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    </div>
    
    <div class="content-block">
        
        <img src="presentation_layer/images/logo.png" class="logo">
        
        <p>This tool is used to automatically create corporate signatures for members.</p>

        <p>To use this tool, please type your exact ArcSec forum username into the form below and click "Generate". The new image will be displayed below the form
        along with the link to the image and generated RSI.com and ARCSEC forum signature blocks.</p>
        
        <p>You can copy the RSI signature block and paste it into your RSI.com profile <a href="https://robertsspaceindustries.com/account/profile">HERE</a>. 
            Paste it into the "Signature" field at the bottom of that page and hit "Apply All Changes".</p>
        
        <p>You can copy the ARCSEC signature block and paste it into your forum profile <a href="http://arcsecurity.shivtr.com/my_account">HERE</a>. 
            Paste it into the "Forum Signature" field at the bottom of that page and hit "Update".</p>

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    
    </div>

    
    <div class="content-block">
        
        <div class="sig_generator_form_bounds">
            <form method="GET" onSubmit="return false;" id="sig_generator_form" name="sig_generator_form">
                
                <center>
                    
                    <input type="hidden" id="action" name="action" value="single">

                    <input type="text" id="handle" name="handle" placeholder="Handle" <?php if($_GET['action'] == 'single') echo 'value="'.$_GET['handle'].'"'; ?>>

                    <br>
                    <br>
                    
                    <a onClick="ProcessSubmit();" id="submit" class="holobtn">
                        <span class="holobtn-top abs-overlay trans-02s">Generate</span>
                        <span class="holobtn-bottom abs-overlay trans-02s"></span>
                    </a>
                </center>
                
            </form>
        </div>

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    
    </div>

    <div class="content-block sig_generator_output" id="sig_generator_output" name="sig_generator_output" style="display:none;">

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    </div>
    
</div>

<?php 
    if($_GET['action'] == 'single')
    {
        echo '<script type="text/javascript">ProcessSubmit();</script>';
    }
?>