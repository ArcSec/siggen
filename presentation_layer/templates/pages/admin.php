
<h1 class="page-title ">

    <span class="top-line"></span>
    <span class="inner-line"></span>
    <span class="icon"></span>
        Arc Security Signature Generator - Admin
    <span class="bottom-line"></span>

</h1>

<div class="panel">
    
    <div class="content-block" style="font-size:80%;">
        <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">Home</a> 
        &gt; <a href="index.php">Signature Generator</a>
        &gt; Admin
        
        <a href="admin.php" style="float:right;">Admin</a> 

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    </div>
    
    <div class="content-block">
        
        <img src="presentation_layer/images/logo.png" class="logo">
        
        <p>Successfully authenticated...</p>

        <p>To use this tool manually, please utilize the form below to generate the desired output. Output will be displayed below the form dynamically.</p>

        <p>To automate this tool or call it from another script, it simply requires two GET parameters: 'action' and 'handle'. 'action' can be either 'all' or 'single' where
        'all' creates signatures for all members and 'single' will create one only for the member specified in the 'handle' parameter.</p>

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    
    </div>

    
    <div class="content-block">
        
        <div class="sig_generator_form_bounds">
            <form method="GET" onSubmit="return false;" id="sig_generator_form" name="sig_generator_form">
                
                <center>
                    
                    <select id="action" name="action" OnChange="ProcessRequestType();">
                        <option value="single">Single Member</option>
                        <option value="all">All Members</option>
                    </select>
                    
                    <br>
                    <br>

                    <input type="text" id="handle" name="handle" placeholder="Handle" style="margin-bottom:50px;">
                    
                    <a onClick="ProcessSubmit();" id="submit" class="holobtn">
                        <span class="holobtn-top abs-overlay trans-02s">Generate</span>
                        <span class="holobtn-bottom abs-overlay trans-02s"></span>
                    </a>
                </center>
                
            </form>
        </div>

        <div class="corner-top-left corner"></div>

        <div class="corner-top-right corner"></div>

        <div class="corner-bottom-left corner"></div>

        <div class="corner-bottom-right corner"></div>
    
    </div>

    <div class="sig_generator_output" id="sig_generator_output" name="sig_generator_output"></div>
    
</div>