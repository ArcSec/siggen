<?php

require('header.php');

if($templates != null)
{
    foreach($templates as $template)
    {
        require(__DIR__.'/../pages/'.$template);
    }
}

require('footer.php');
