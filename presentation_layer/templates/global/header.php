<!DOCTYPE html>

<html>
    
    <head>
        
        <title>ArcSec - Signature Generator</title>
        
        <meta charset="UTF-8">
        <meta http-equiv="content-type" content="text/html; charset=utf-8;" />
        <meta http-equiv="content-style-type" content="text/css" />
        <meta http-equiv="content-language" content="en" />
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="resource-type" content="document" />
        <meta name="distribution" content="global" />
        
        <link href='http://fonts.googleapis.com/css?family=Electrolize' rel='stylesheet' type='text/css'>
        
        <?php
        
        if($styles != null)
        {
            foreach($styles as $style)
            {
echo '      <link rel="stylesheet" href="presentation_layer/css/'.$style.'">
';
            }
        }
        
        if($scripts != null)
        {
            foreach($scripts as $script)
            {
echo '      <script type="text/javascript" src="presentation_layer/js/'.$script.'"></script>
';
            }
        }
        
        ?>
        
    </head>
    
    <body>
        
        <div class="content">
            
            <div class="bounds">
