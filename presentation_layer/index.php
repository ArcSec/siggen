<?php

require_once('php/SigGeneratorScripts.php');

switch(strtolower($_GET['action']))
{
    case 'all':
        CreateAll();
        break;
    
    case 'single':
        CreateSingle($_GET['handle']);
        break;
    
    default:
        break;
}

$templates[] = 'front_page.php';

$styles[] = 'global.css';

$scripts[] = 'SignatureGenerator.js';

require_once('templates/global/template.php');
