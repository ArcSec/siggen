<?php

require_once('SigGeneratorScripts.php');

switch(strtolower($_GET['action']))
{
    
    case 'all':
        $output = CreateAll();
        
        if($output[0] != null)
        {
            $full_path = ImageUrl($output[0]);
            
            preg_match('|^(.+)/.*$|',$full_path, $matches);
            $full_path = $matches[1];
        }
        
        echo '<center>
                Created all signatures successfully.
                <br><br>
                <a href="'.$full_path.'" target="_blank">View All</a>
            </center>
            
            <div class="corner-top-left corner"></div>

            <div class="corner-top-right corner"></div>

            <div class="corner-bottom-left corner"></div>

            <div class="corner-bottom-right corner"></div>';
        break;
    
    case 'single':
        $path = CreateSingle($_GET['handle']);
        
        if($path != null)
        { 
            $full_path = ImageUrl($path);
            
            echo 
            '<center>
                <table style="width:100%;">
                    <tr>
                        <td style="text-align:right;">
                            Direct Link:
                        </td>
                        <td style="text-align:center;">
                            <a href="'.$full_path.'" target="_blank">'.$full_path.'</a><br>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">
                            RSI:
                        </td>
                        <td style="text-align:center;">
                            <span style="font-size:80%;color:rgb(0,255,255);">'.
                                '&lt;center&gt;&lt;a href="http://'.(($_SERVER['HTTP_HOST'] != 'christiansibo.com')?$_SERVER['HTTP_HOST']:'arcsecurity.org').'"&gt;&lt;img src="'.$full_path.'"&gt;&lt;/a&gt;&lt;/center&gt;'.
                            '</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">
                            ARCSEC:
                        </td>
                        <td style="text-align:center;">
                            <span style="font-size:80%;color:rgb(0,255,255);">'.
                                '[img overlay=true]'.$full_path.'[/img]'.
                            '</span>
                        </td>
                    </tr>
                </table>
                <img src="'.$path.'">
            </center>
            
            <div class="corner-top-left corner"></div>

            <div class="corner-top-right corner"></div>

            <div class="corner-bottom-left corner"></div>

            <div class="corner-bottom-right corner"></div>';
        }
        else
        {
            echo '<center>!! Error !!</center>

            <div class="corner-top-left corner"></div>

            <div class="corner-top-right corner"></div>

            <div class="corner-bottom-left corner"></div>

            <div class="corner-bottom-right corner"></div>';
        }
        break;
    
    default:
        break;
}