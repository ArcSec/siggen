<?php

require_once(__DIR__.'/../../application_layer/classes/TemplateBuilder.php');
require_once(__DIR__.'/../../application_layer/classes/ImageCreator.php');

function CreateAll()
{
    $Tb = new TemplateBuilder();
    $members = $Tb->GetMembers();

    $output = null;
    
    foreach($members as $member)
    {
        $output[] = CreateSingle($member);
    }
    
    return $output;
}

function CreateSingle($handle)
{
    set_time_limit(30);
    
    $Tb = new TemplateBuilder();
    
    if(is_array($handle))
    {
        $member = $handle;
    }
    else
    {
        $member = $Tb->GetMember($handle);
    }
    
    $output_path = '../../../sig/'.urlencode(strtolower($member['display_name']));

    $template = $Tb->CreateTemplate($member);
    
    if($template == null)
    {
        return null;
    }
    
    $Creator = new ImageCreator($template);
    $path = $Creator->OutputImage($Creator->RenderImage(), $output_path);
    
    return 'presentation_layer/'.substr($path,3);
}

function ImageUrl($path) 
{
    // Get full path of the image
    $full_path = realpath('../../'.$path);

    // Replace Windows style path delimiters with Linux style delimiters
    $full_path = str_replace('\\', '/', $full_path);

    // Replace the server root dir with the root URL of the server
    $full_path = str_replace($_SERVER['DOCUMENT_ROOT'], $_SERVER['HTTP_HOST'], $full_path);

    // Prepend the protocol
    $full_path = 'http://'.$full_path;
    
    return $full_path;
}