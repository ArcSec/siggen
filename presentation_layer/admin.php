<?php

require_once('php/SigGeneratorScripts.php');
require_once(__DIR__.'/../application_layer/classes/LoginClient.php');

$LoginClient = new LoginClient();

if($_COOKIE['siggen_auth'])   
{
    $json_stream = @file_get_contents('http://arcsecurity.shivtr.com/members.json?auth_token='.$_COOKIE['siggen_auth']);
    $query_data = json_decode($json_stream, true);
    if($query_data['members'] != null)
    {
        $templates[] = 'admin.php';
    }
}

if($templates == null)
{
    if($_POST['username'] && $_POST['password'])
    {
        $key = $LoginClient->LoginShivtr($_POST['username'], $_POST['password'], 'arcsecurity');
        setcookie('siggen_auth',$key);
        $templates[] = 'admin.php';
    }
    else
    {
        $templates[] = 'admin_login.php';
    }
}

$styles[] = 'global.css';

$scripts[] = 'SignatureGenerator.js';

require_once('templates/global/template.php');