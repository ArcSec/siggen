
function ProcessSubmit()
{
    RetrieveImage();
    return false;
}

function BuildQueryString(base)
{
    var controls = document.sig_generator_form.elements;
    
    var url = base + '?';
    
    for(var i = 0; i < controls.length; i++) 
    {
        if(controls[i].style.display != 'none'
            && controls[i].hidden == false
            && controls[i].type != 'submit'
            && controls[i].type != 'textarea'
            && controls[i].name != 'location')
        {
            if(i > 0)
            {
                url += '&';
            }
            
            url += controls[i].name + '=' + escape(controls[i].value);
        }
    }

    return url;
}

function RetrieveImage()
{
    document.getElementById("sig_generator_output").style.display = 'none';

    document.getElementById("submit").innerHTML = '<div class="circle"></div><div class="circle1"></div>';

    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("sig_generator_output").innerHTML = xmlhttp.responseText;
            document.getElementById("sig_generator_output").style.display = 'block';

            document.getElementById("submit").innerHTML = '<a onClick="ProcessSubmit();" id="submit" class="holobtn"><span class="holobtn-top abs-overlay trans-02s">Generate</span><span class="holobtn-bottom abs-overlay trans-02s"></span></a>';
        }
    }
    
    var base_url = document.URL;
    base_url = base_url.substring(0, base_url.lastIndexOf('/'));
    base_url += '/';
    
    var url = BuildQueryString(base_url + 'presentation_layer/php/CreateImage.php');
    
    xmlhttp.open("GET",url,true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send();
}

function ProcessRequestType()
{
    var action = document.getElementById("action").value;
    
    switch(action)
    {
        default:
        case 'single':
            document.getElementById("handle").style.display = 'block';
            break;
        case 'all':
            document.getElementById("handle").style.display = 'none';
            break;
    }
}

