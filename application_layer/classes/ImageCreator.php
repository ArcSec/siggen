<?php

class Color
{
    private $r;
    private $g;
    private $b;
    
    function Color($r, $g, $b)
    {
        $this->__set('r', $r);
        $this->__set('g', $g);
        $this->__set('b', $b);
    }
    
    public function __set($member, $value)
    {
        $this->$member = $this->ValidateColorValue($value);
    }
    
    public function __get($member)
    {
        return $this->$member;
    }
    
    protected function ValidateColorValue($value)
    {
        if(!isset($value)
            || $value == null
            || !is_numeric($value))
        {
            return 0;
        }
        
        if($value < 0)
        {
            return 0;
        }
        
        if($value > 255)
        {
            return 255;
        }
        
        return $value;
    }
}

class Position
{
    private $x;
    private $y;
    
    function Position($x, $y)
    {
        $this->__set('x', $x);
        $this->__set('y', $y);
    }
    
    public function __set($member, $value)
    {
        $this->$member = $this->ValidatePositionValue($value);
    }
    
    public function __get($member)
    {
        return $this->$member;
    }
    
    protected function ValidatePositionValue($value)
    {
        if(!isset($value)
            || $value == null
            || !is_numeric($value))
        {
            return 0;
        }
        
        return $value;
    }
}

class Size
{
    private $width;
    private $height;
    
    function Size($width = 1, $height = 1)
    {
        $this->__set('width', $width);
        $this->__set('height', $height);
    }
    
    public function __set($member, $value)
    {
        $this->$member = $this->ValidateSizeValue($value);
    }
    
    public function __get($member)
    {
        return $this->$member;
    }
    
    protected function ValidateSizeValue($value)
    {
        if(!isset($value)
            || $value == null
            || !is_numeric($value))
        {
            return 0;
        }
        
        return $value;
    }
}

class ImageCreator
{
    private $overlayObjects;
    private $render_size;
    private $display_size;
    private $format;
    private $quality;
    private $do_crop;
    
    //DEFAULT CONSTRUCTOR
    function ImageCreator($json_stream)
    {
        $this->overlayObjects = array();
        $this->render_size = new Size();
        $this->display_size = new Size();
        $this->format = null;
        $this->quality = 100;
        $this->do_crop = false;
        
        $this->InputObjects($json_stream);
    }
    
    public function AddOverlayObject($object, $order = -1)
    {
        // Check for valid parameters
        if($object == null)
        {
            return false;
        }
        
        if($order >=0)
            $this->InsertAt($object, $order);
        else
            $this->InsertAt($object, count($this->overlayObjects));
        
        return true;
    }
    
    private function InsertAt($object, $insertionPoint = 0)
    {
        // Check for valid parameters
        if(!isset($insertionPoint) || !is_numeric($insertionPoint))
        {
            return false;
        }
        
        if($insertionPoint >= count($this->overlayObjects))
        {
            $this->overlayObjects[count($this->overlayObjects)] = $object;
        }
        else
        {
            // Shift the entire list right from the end of the list to the insertion point
            for($i=count($this->overlayObjects) - 1; $i >= $insertionPoint; $i--)
            {
                $this->overlayObjects[$i + 1] = $this->overlayObjects[$i];
            }

            // Insert the new element
            $this->overlayObjects[$insertionPoint] = $object;
        }
        
        return true;
    }
    
    private function RemoveAt($deletionPoint)
    {
        // Check for valid parameters
        if(!isset($deletionPoint) || !is_numeric($deletionPoint))
        {
            return false;
        }
        
        // Shift the entire list left from the deletion point to the end of the list
        for($i=$deletionPoint; $i < array_count_values($this->overlayObjects); $i++)
        {
            $this->overlayObjects[$i] = $this->overlayObjects[$i + 1];
        }
        
        // Unset the last element of the list as it is a duplicate after shifting
        unset($this->overlayObjects[array_count_values($this->overlayObjects)]);
        
        return true;
    }
    
    private function FormatToImageType()
    {
        switch($this->format)
        {
            default:
            case "png":
                $this->format = IMAGETYPE_PNG;
                break;
            case "jpg":
            case "jpeg":
                $this->format = IMAGETYPE_JPEG;
                break;
            case "gif":
                $this->format = IMAGETYPE_GIF;
                break;
        }
    }
    
    private function ValidateQualitySetting()
    {
        switch($this->format)
        {
            default:
            case IMAGETYPE_PNG:
                $this->quality = max(0, $this->quality);
                $this->quality = min(9, $this->quality);
                break;
            case IMAGETYPE_JPEG:
                $this->quality = max(0, $this->quality);
                $this->quality = min(100, $this->quality);
                break;
            case IMAGETYPE_GIF:
                $this->quality = null;
                break;
        }
    }
    
    private function ValidateFormatSettings()
    {
        $this->FormatToImageType();
        $this->ValidateQualitySetting();
    }
    
    public function InputObjects($json_stream)
    {
        unset($this->overlayObjects);
        $this->overlayObjects = array();
        
        if(is_string($json_stream))  
            $json_stream = json_decode($json_stream, true);
        
        $this->display_size = new Size($json_stream['settings']['display_resolution']['width'], $json_stream['settings']['display_resolution']['height']);
        $this->render_size = new Size($json_stream['settings']['render_resolution']['width'], $json_stream['settings']['render_resolution']['height']);
        $this->format = $json_stream['settings']['format'];
        $this->quality = $json_stream['settings']['quality'];
        $this->do_crop = $json_stream['settings']['crop'];
        
        $this->ValidateFormatSettings();
        
        foreach($json_stream['objects'] as $object)
        {
            $position = new Position($object['position']['x'], $object['position']['y']);
            $angle = $object['angle'];
            $alpha = $object['alpha'];

            if($object['type'] == 'image')
            {
                $size = new Size($object['size']['width'], $object['size']['height']);

                $object = new OverlayImage($object['url']);
                $object->ResizeByPixels($size->width, $size->height);
            }
            else if($object['type'] == 'text')
            {
                $size = $object['size'];
                $color = new Color($object['color']['r'], $object['color']['g'], $object['color']['b']);

                $object = new OverlayText($object['text']);
                $object->size = $size;
                $object->color = $color;
            }

            $object->position = $position;
            $object->angle = $angle;

            $this->AddOverlayObject($object);

            $i++;
        }
    }
    
    public function RenderImage()
    {
        $render_image = new OverlayImage();
        $render_image->CreateTransparentImage($this->render_size);
        
        //$px = (imagesx($im) - 7.5 * strlen($string)) / 2;
        
        foreach($this->overlayObjects as $object)
        {
            if(get_class($object) == OverlayText)
            {
                $color = imagecolorallocatealpha($render_image->image, $object->color->r, $object->color->g, $object->color->b, $object->alpha);
                
                $font = realpath(dirname(__FILE__) . '/../../presentation_layer/fonts/Orbitron-Medium.ttf');
				
                imagettftext($render_image->image, $object->size, $object->angle, $object->position->x, $object->position->y, 
                        $color, $font, $object->text);
            }
            else if(get_class($object) == OverlayImage)
            {
                imagecopy($render_image->image, $object->image, $object->position->x, $object->position->y, 0, 0, 
                        $object->size->width, $object->size->height);
            }
        }
        
        if($this->do_crop)
        {
            $render_image->image = imagecropauto($render_image->image);
        }
        
        return $render_image->image;
    }
    
    public function OutputImage($image, $path = null)
    {
        $display_image = new OverlayImage();
        $display_image->CreateTransparentImage($this->display_size);
        
        imagecopyresized(
                $display_image->image, $image, 0, 0, 0, 0, 
                $display_image->size->width, $display_image->size->height, 
                imagesx($image), imagesy($image));
        
        if($path == null)
            header('Content-type: ' . image_type_to_mime_type($this->format));
        
        switch($this->format)
        {
            default:
            case IMAGETYPE_PNG:
                $path.='.png';
                imagepng($display_image->image, $path, $this->quality);
                break;
            case IMAGETYPE_JPEG:
                $path.='.jpg';
                imagejpeg($display_image->image, $path, $this->quality);
                break;
            case IMAGETYPE_GIF:
                $path.='.gif';
                imagegif($display_image->image, $path);
                break;
        }
        
        imagedestroy($display_image->image);
        unset($display_image);
        
         if($path != null)
             return $path;
    }
}

abstract class OverlayObject
{
    public $position;
    private $angle;
    
    //DEFAULT CONSTRUCTOR
    function OverlayObject()
    {
        $this->position = new Position(0,0);
        $this->angle = 0;
    }
    
    public function __set($member, $value)
    {
        if($member == 'angle')
        {
            $value = $this->ValidateAngle($value);
        }
        
        $this->$member = $value;
    }
    
    public function __get($member)
    {
        return $this->$member;
    }
    
    protected function ValidateAngle($value)
    {
        if(!isset($value)
            || $value == null
            || !is_numeric($value))
        {
            return 0;
        }
        
        return $value;
    }
}

class OverlayText extends OverlayObject
{
    public $text;
    private $size;
    public $font;
    private $alpha;
    public $color;
    
    //DEFAULT CONSTRUCTOR
    function OverlayText($text = '')
    {
        $this->text = $text;
        $this->size = 14;
        $this->font = '';
        $this->alpha = 0;
        $this->color = new Color(0,0,0);
    }
    
    public function __set($member, $value)
    {
        if($member == 'size')
        {
            $value = $this->ValidateSize($value);
        }
        if($member == 'alpha')
        {
            $value = $this->ValidateAlpha($value);
        }
        
        $this->$member = $value;
    }
    
    public function __get($member)
    {
        return $this->$member;
    }
    
    protected function ValidateSize($value)
    {
        if(!isset($value)
            || $value == null
            || !is_numeric($value))
        {
            return 14;
        }
        
        if($value < 0)
        {
            return 14;
        }
        
        return $value;
    }
    
    protected function ValidateAlpha($value)
    {
        if(!isset($value)
            || $value == null
            || !is_numeric($value))
        {
            return 0;
        }
        
        if($value < 0)
        {
            return 0;
        }
        
        if($value > 127)
        {
            return 127;
        }
        
        return $value;
    }
}

class OverlayImage extends OverlayObject
{
    private $image;
    public $size;
    
    //DEFAULT CONSTRUCTOR
    function OverlayImage($image = null)
    {
        if($image != null)
        {
            $this->__set('image', @imagecreatefrompng($image));
        }
        else
        {
            $this->size = new Size(100, 100);
            $this->image = $this->CreateTransparentImage($this->size);
        }
    }
    
    public function __set($member, $value)
    {
        if($member == 'image')
        {
            imageAlphaBlending($value, true);
            imageSaveAlpha($value, true);
            
            $width=imageSX($value);
            $height=imageSY($value);
            $this->size = new Size($width, $height);
        }
        if($member == 'angle')
        {
            $value = $this->ValidateAngle($value);
            $this->RotateImage($value);
        }
        
        $this->$member = $value;
    }
    
    public function __get($member)
    {
        return $this->$member;
    }
    
    public function ResizeByPercentage($width, $height)
    {
        $this->size = new Size($this->size->width * ($width/100), $this->size->height * ($height/100));
        
        $this->ResizeByPixels($this->size->width, $this->size->height);
    }
    
    public function ResizeByPixels($width, $height)
    {
        $this->size = new Size($width, $height);
        
        $width = imagesx($this->image);
        $height = imagesy($this->image);
        
        $temp_image = $this->image;
        $this->CreateTransparentImage($this->size);
        
        imagecopyresized($this->image, $temp_image, 0, 0, 0, 0, 
                        $this->size->width, $this->size->height, $width, $height);
    }
    
    public function CreateTransparentImage($size)
    {
        $this->__set('image', imagecreatetruecolor($size->width, $size->height));
        
        $trans_colour = imagecolorallocatealpha($this->image, 0, 0, 0, 127);
        imagefill($this->image, 0, 0, $trans_colour);
    }
    
    protected function RotateImage($angle)
    {
        $trans_colour = imagecolorallocatealpha($this->image, 0, 0, 0, 127);
        
        $this->__set('image', imagerotate($this->image, $angle, $trans_colour));
    }
}