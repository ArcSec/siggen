<?php

require('LoginClient.php');

class TemplateBuilder
{
    private $Proxy;
    private $rank_map = array(
        'admiral' => array(
            'displays_by_name' => true,
            'display_rank' => 'Admiral',
            'description' => 'Chief Operations Officer',
            'insignia' => array(
                'url' => 'admiral.png',
                'width' => 103,
                'height' => 104,
                ),
            ),
        'vice-admiral (ceo)' => array(
            'displays_by_name' => true,
            'display_rank' => 'Vice Admiral',
            'description' => 'Chief Executive Officer',
            'insignia' => array(
                'url' => 'vice_admiral.png',
                'width' => 73,
                'height' => 107,
                ),
            ),
        'commodore' => array(
            'displays_by_name' => false,
            'display_rank' => 'Commodore',
            'description' => null,
            'insignia' => array(
                'url' => 'commodore.png',
                'width' => 68,
                'height' => 107,
                ),
            ),
        'commander' => array(
            'displays_by_name' => false,
            'display_rank' => 'Squadron Commander',
            'description' => null,
            'insignia' => array(
                'url' => 'squad_commander.png',
                'width' => 73,
                'height' => 106,
                ),
            ),
        'cadet' => array(
            'displays_by_name' => false,
            'display_rank' => 'Cadet',
            'description' => null,
            'insignia' => array(
                'url' => null,
                'width' => 0,
                'height' => 0,
                ),
            ),
        'recruit' => array(
            'displays_by_name' => false,
            'display_rank' => 'Recruit',
            'description' => null,
            'insignia' => array(
                'url' => null,
                'width' => 0,
                'height' => 0,
                ),
            ),
        );
    
    private $members;
    private $ranks;
    
    public function __construct()
    {
        $this->Proxy = new LoginClient();
        
        $this->GetData();
    }
    
    public function Authenticate()
    {
        return $this->Proxy->LoginShivtr();
    }
    
    public function GetData()
    {
        $this->members = $this->GetMembers();
        
        $this->ranks = $this->GetRanks();
    }
    
    public function GetMembers()
    {
        $key = $this->Authenticate();
        
        $json_stream = $this->Proxy->GetPage('http://arcsecurity.shivtr.com/members.json?auth_token='.$key);
        
        $query_data = json_decode($json_stream, true);
        
        $data = $query_data['members'];
        
        return $data;
    }
    
    public function GetRanks()
    {
        $key = $this->Authenticate();
        
        $json_stream = $this->Proxy->GetPage('http://arcsecurity.shivtr.com/ranks.json?auth_token='.$key);
        
        $query_data = json_decode($json_stream, true);
        
        foreach($query_data['ranks'] as $rank)
        {
            $data[$rank['id']] = $rank['name'];
        }
        
        return $data;
    }
    
    public function GetMember($handle)
    {
        $members = $this->GetMembers();
        
        $found = false;
        foreach($members as $member)
        {
            if(strtolower($member['display_name']) == strtolower($handle))
            {
                $id = $member['id'];
                $found = true;
            }
        }
        
        if(!$found)
        {
            return null;
        }
        
        $key = $this->Authenticate();
        
        $json_stream = $this->Proxy->GetPage('http://arcsecurity.shivtr.com/members/'.$id.'.json?auth_token='.$key);
        
        $query_data = json_decode($json_stream, true);
        
        $data = $query_data['member'];
        
        return $data;
    }
    
    public function CreateTemplate($data)
    {
        if($data == null)
        {
            return null;
        }
        
        $username = $data['display_name'];
        $rank = strtolower($this->ranks[$data['rank_id']]);
        
        $template = json_decode(file_get_contents(__DIR__.'/../template.json'), true);
        
        if(!$this->rank_map[$rank]['displays_by_name'])
        {
            $template['objects'][] = array(
                'type' => 'text',
                'text' => $this->rank_map[$rank]['display_rank'],
                'position' => array(
                    'x' => 130,
                    'y' => 75,
                    ),
                'size' => 18,
                'angle' => 0,
                'color' => array(
                    'r' => 255,
                    'g' => 255,
                    'b' => 255,
                    ),
                );

            $template['objects'][] = array(
                'type' => 'text',
                'text' => ucwords($username),
                'position' => array(
                    'x' => 130,
                    'y' => 105,
                    ),
                'size' => 18,
                'angle' => 0,
                'color' => array(
                    'r' => 255,
                    'g' => 255,
                    'b' => 255,
                    ),
                );
        }
        else
        {
            $template['objects'][] = array(
                'type' => 'text',
                'text' => $this->rank_map[$rank]['display_rank'].' '.ucwords($username),
                'position' => array(
                    'x' => 130,
                    'y' => 75,
                    ),
                'size' => 18,
                'angle' => 0,
                'color' => array(
                    'r' => 255,
                    'g' => 255,
                    'b' => 255,
                    ),
                );

            $template['objects'][] = array(
                'type' => 'text',
                'text' => $this->rank_map[$rank]['description'],
                'position' => array(
                    'x' => 130,
                    'y' => 105,
                    ),
                'size' => 18,
                'angle' => 0,
                'color' => array(
                    'r' => 255,
                    'g' => 255,
                    'b' => 255,
                    ),
                );
        }
        
        if($this->rank_map[$rank]['insignia']['url'] != null)
        {
            $template['objects'][] = array(
                'type' => 'image',
                'url' => 'http://christiansibo.com/arcsec/sig_generator/presentation_layer/images/insignias/'.$this->rank_map[$rank]['insignia']['url'],
                'position' => array(
                    'x' => 25,
                    'y' => 25,
                    ),
                'size' => array(
                    'width' => $this->rank_map[$rank]['insignia']['width'],
                    'height' => $this->rank_map[$rank]['insignia']['height']
                    ),
                'angle' => 0,
                );
        }
        
        return $template;
    }
    
    public function GetInsignia($data)
    {
        
    }
    
}
