<?php

$_SETTINGS = array(
    
    // Proxy client settings
    'clients' => array(
        'rsi' => array(
            'username' => '',    // Handle of the RSI account you wish to use
            'password' => '',    // MD5 of the password of the RSI account you wish to use
        ),
        
        'shivtr' => array(
            'username' => '',    
            'password' => '',
            'id' => 'arcsecurity',
        ),
    ),
	
);